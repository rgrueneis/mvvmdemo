﻿namespace ArtistDbLib;

public class ArtistContext
{
  public List<Artist> Artists { get; set; } = new();
  public List<Song> Songs { get; set; } = new();

  public ArtistContext()
  {
    InitArtistAndHisSongs("Yello", "Bostich", "Domingo", "I Love You");
    InitArtistAndHisSongs("OMD", "Enola Gay", "Electricity", "Genetic Engineering");
    InitArtistAndHisSongs("Ultravox", "Vienna", "The Wall", "Reap The Wild Wind", "Astradyne");
    InitArtistAndHisSongs("Yazoo", "Don't Go", "Nobody's Diary", "Goodbye Seventies", "Only You");
  }

  private void InitArtistAndHisSongs(string artistName, params string[] songNames)
  {
    var artist = new Artist { Name = artistName };
    Artists.Add(artist);
    var songs = songNames.Select(x => new Song { Name = x, Artist = artist });
    Songs.AddRange(songs);
    artist.Songs.AddRange(songs);
  }
}
