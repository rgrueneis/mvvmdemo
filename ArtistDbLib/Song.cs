﻿namespace ArtistDbLib;

public class Song
{
  public string? Name { get; set; }
  public Artist? Artist { get; set; }
  public override string ToString() => Name ?? "-";
}
