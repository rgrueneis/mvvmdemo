﻿namespace ArtistDbLib;

public class Artist
{
  public string? Name { get; set; }
  public List<Song> Songs { get; set; } = new();
  public override string ToString() => Name ?? "-";
}
